import React, {Component} from 'react';
import Linkify from 'react-linkify';

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

class BotMsg extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        let {msgTxt, timestamp, msgNum, linkList, msgType} = this.props;

        // CSS classes
        const msgClasses = [
            "chtBot_mxWdth--85per",
            "chtBot_flt--left",
            "chtBot_mrgnAll--2per",
            "chtBot_mrgnRght--25per",
            "chtBot_bgClr--lghtBlue",
            "chtBot_brdrRdTpLft--15px",
            "chtBot_brdrRdTpRght--15px",
            "chtBot_brdrRdBtmRght--15px",
            "chtBot_lineHght--1pt5",
            "chtBot_pddBtm--5px",
            "chtBot_pddTp--10px",
            "chtBot_pddRgt--15px",
            "chtBot_pddLft--15px"
        ];
        const timestampClasses = [
            "chtBot_fntSz--9px",
            "chtBot_txtClr--gray",
            "chtBot_pddBtm--5px",
            "chtBot_pddTp--10px",
            "chtBot_dsply--block"
        ];

        return(
          <div name={'msgNum-${msgNum}'} className="msg-bubble">
            <div className="msg-info">
              <div className="msg-info-name">Elector</div>
              <div className="msg-info-time">{timestamp}</div>
            </div>
            <div className="msg-text">
                {msgType == 'option'
                    ?
                        <ul>{
                                msgTxt.map((linkObj, i) => { 
                                    return <li key={i}><a href={null} target="_blank">{linkObj.value.input.text}</a></li>
                                })
                            }
                        </ul>
                    :
                        msgType == 'info_votacion'
                        ?    
                                <div>
                                    <p>Hola {jsUcfirst(msgTxt.data[2]).replace(/ .*/,'')}, esta es la info:</p>
                                    <p>Ciudad: {jsUcfirst(msgTxt.data[3]).replace(/\b\w/g, l => l.toUpperCase())}</p>
                                    <p>Colegio: {jsUcfirst(msgTxt.data[4]).replace(/\b\w/g, l => l.toUpperCase())}</p>
                                </div>
                        :
                            <Linkify properties={{target: '_blank'}}>{msgTxt}</Linkify>
                }
            </div>
          </div>
         )
        /*
        */
    }
}

export default BotMsg;