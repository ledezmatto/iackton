import React, {Component} from 'react'

import InitBtn from './InitBtn'
import ChatBotWindow from './ChatWindow'

class ChatBotContainer extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            chatWindowVisible: true
        }
    }

    // Toggles visibility of chat window
    toggleChatWindow() {
        this.setState({chatWindowVisible: !this.state.chatWindowVisible});
    }

    render() {
        let {chatWindowVisible} = this.state;

        return(
            <ChatBotWindow toggleChatWindow={this.toggleChatWindow.bind(this)} />
        )
    }
}

export default ChatBotContainer;
