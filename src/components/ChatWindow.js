import React, {Component} from 'react';
import axios from 'axios';
import * as Scroll from 'react-scroll'

import ChatHeader from './ChatHeader';
import ChatDialog from './ChatDialog';
import ChatInput from './ChatInput';
const {DEV_HOST_URL, LOCAL_HOST_URL} = require('../../static_store');

const scroller = Scroll.scroller;

function jsUcfirst(string) 
{
    console.log(string.charAt(0).toUpperCase() + string.slice(1).toLowerCase())
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

class ChatBotWindow extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            chatMsgs: [],
            session_id: null,
            showLoader: true
        }
    }

    componentDidMount() {
        let {session_id} = this.state;
        let that = this;

        // Initiate the conversation once chat window loads
        if(!session_id) {
            axios.get( DEV_HOST_URL + '/api/new-session', {})
            .then((response) => {
                this.setState({session_id: response.data.session_id});
                this.postMsgWrapper(
                    'Hola',
                    response.data.session_id,
                    that
                );
            });  
        }
    }

    // Wrapper function for POST request to send user input
    postMsgWrapper( userMsg, session_id, thisContext ) {
        let {chatMsgs} = this.state;

        return axios.post( DEV_HOST_URL + '/api/message', {
            input: {text: userMsg}, session_id: session_id
        }).then(function (response) {
            console.log(response.data.output.entities.length)
            if(response.data.output.entities.length > 0){
                if(response.data.output.entities[0].entity == 'sys-number'){
                    var cedula = response.data.output.entities[0].value;
                    console.log(cedula)
                    // Consultamos lugar de votación
                    axios.get('http://190.128.194.162/ws/rcp/consulta2.php?cc='+cedula+'&op=99999').then(function (response) {
                        console.log(response)
                        const newBotMsg = {
                            msgTxt: response,
                            msgType: 'info_votacion',
                            msgTimestamp: new Date().toLocaleTimeString(),
                            isBot: true,
                            msgNum: chatMsgs.length + 1
                        }
                        chatMsgs.push(newBotMsg);
                        thisContext.setState({chatMsgs, showLoader: false}, () => {
                            thisContext.scrollToMsg(`msgNum-${newBotMsg.msgNum}`);
                        });
                    });
                }
            }else{
                response.data.output.generic.map((generic, i) => { 
                    if(generic.response_type == 'options'){
                        const newBotMsg = {
                            msgType: generic.response_type,
                            msgOptions: generic.options,
                            msgTimestamp: new Date().toLocaleTimeString(),
                            isBot: true,
                            msgNum: chatMsgs.length + 1
                        }
                        chatMsgs.push(newBotMsg);
                        thisContext.setState({chatMsgs, showLoader: false}, () => {
                            thisContext.scrollToMsg(`msgNum-${newBotMsg.msgNum}`);
                        });
                    }else{
                        const newBotMsg = {
                            msgTxt: generic.text,
                            msgType: generic.response_type,
                            msgOptions: generic.options,
                            msgTimestamp: new Date().toLocaleTimeString(),
                            isBot: true,
                            msgNum: chatMsgs.length + 1
                        }
                        chatMsgs.push(newBotMsg);
                        thisContext.setState({chatMsgs, showLoader: false}, () => {
                            thisContext.scrollToMsg(`msgNum-${newBotMsg.msgNum}`);
                        });
                    }
                })
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    // Handles adding user message to dialog and calls postMsgWrapper or initiates feedback sequence
    handleUserMessage( userMsg ) {
        let {chatMsgs, session_id, inFeedbackSequence} = this.state;
        let that = this;
        let newUserMsg = {
            msgTxt: userMsg,
            msgTimestamp: new Date().toLocaleTimeString(),
            isBot: false,
            msgNum: chatMsgs.length + 1
        }
        chatMsgs.push(newUserMsg);
        this.setState({chatMsgs}, () => {
            this.scrollToMsg(`msgNum-${newUserMsg.msgNum}`);
            this.setState({showLoader: true});
        });
        this.postMsgWrapper(
            userMsg,
            session_id,
            that
        );
    }

    // Auto-scrolls dialog to top of messages
    scrollToMsg(elemName) {
        scroller.scrollTo(elemName, {
            duration: 400,
            delay: 0,
            smooth: true,
            containerId: 'dialog',
            offset: -325
        });
    }

    render() {
        let {chatMsgs, showLoader} = this.state;
        
        // CSS classes
        const dialogWrapClasses = [
            "chtBot_bottom-10--right-10",
            "chtBot_fntFm--sanSer",
            "chtBot_fntSz--12px",
            "chtBot_brdrRdAll--10px",
            "chtBot_wdth--340px",
            "chtBot_hght--478px",
            "chtBot_bgClr--white",
            "chtBot_boxShdw--blue"
        ];

        return(
            <section className="msger">
                <ChatHeader toggleChatWindow={() => this.props.toggleChatWindow()} />
                <ChatDialog chatMsgs={chatMsgs} showLoader={showLoader}/>
                <ChatInput handleUserMessage={this.handleUserMessage.bind(this)} />
            </section>
        )
    }
}

export default ChatBotWindow;
