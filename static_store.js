const DEV_HOST_URL = 'http://elector.mybluemix.net';
const LOCAL_HOST_URL = 'http://localhost:4000';

module.exports = {
    DEV_HOST_URL,
    LOCAL_HOST_URL
};
